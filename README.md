
# Curso Básico BigQuery - Leega

## Preparação do Ambiente

* 1 - Criar DataSet no BigQuery
    * Nome do DataSet no padrão ```curso_bigquery_[iniciais_do_nome] (sem as chaves [])```
    * Exemplo no meu caso Rubens Mussi Cury - ```curso_bigquery_rmc``` 

* 2 - Carregar arquivo do GCS no GBQ

**Arquivos estão disponíveis no Cloud Storage no bucket** 
~~~
curso-bigquery-leega\airline_id_codes.json
curso-bigquery-leega\flights_jfk_lax.json
~~~

## Conceitos Básicos de BigQuery

* 1 - Padrão de Linguagem é ANSI-SQL  
    * **CTRL pressionado** - highlight na tabela  
    * **CTRL + E** - Executa somente a seleção   
    * **CTRL + ENTER** - Executa tudo 
    * **ALT pressionado** - permite seleção vertical
* 2 - Cobrança é por processamento  
    * Atualmente custo entre USD 5/TB para processamento e USD 0.02/TB para armazenamento
    * Não há cobrança para ingestão de dados
* 3 - Quanto mais colunas, mais caro fica  
~~~sql
SELECT * FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
SELECT arrival_schedule, departure_delay, departure_actual FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
SELECT arrival_schedule FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
~~~
* 4 - Limitar o resultado utilizando LIMIT ou WHERE **não altera o valor cobrado** (full-scan)  
~~~sql
SELECT date, departure_airport, airline_code FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
SELECT date, departure_airport, airline_code FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax` WHERE airline_code = 20363
SELECT date, departure_airport, airline_code FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax` WHERE airline_code = 20363 LIMIT 10
~~~
* 5 - Caso seja necessário reutilizar um mesmo resultado **mais de uma vez** considerar a criação de uma tabela temporária
~~~sql
-- Imagine que você deseja realizar muitas operações só para os voos que chegam no estado de Illinois.
CREATE OR REPLACE TABLE `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax_arrival_il`
    OPTIONS(EXPIRATION_TIMESTAMP=TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY))
AS 
  SELECT * FROM `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax` WHERE arrival_state  = "IL"
~~~ 
* 6 - BigQuery aceita nativamente tipo de dados GEO
~~~sql
SELECT 
  ST_GeogPoint(departure_lon, departure_lat) AS PONTO_PARTIDA,
  ST_GeogPoint(arrival_lon, arrival_lat) AS PONTO_CHEGADA
FROM 
  `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
~~~
* 7 - BigQuery permite o uso de matrizes
~~~sql
WITH 
PARTICIPANTES_MATRIZ AS
(
  SELECT PARTICIPANTE FROM UNNEST(["ALUNO 1", "ALUNO 2", "ALUNO 3", "ALUNO 4", "ALUNO 5", "ALUNO 6"]) AS PARTICIPANTE
),
PARTICIPANTES_UNION AS
(
  SELECT "ALUNO 1" AS PARTICIPANTE UNION ALL
  SELECT "ALUNO 2" UNION ALL
  SELECT "ALUNO 3" UNION ALL
  SELECT "ALUNO 4" UNION ALL
  SELECT "ALUNO 5" UNION ALL
  SELECT "ALUNO 6"
)

SELECT PARTICIPANTE FROM PARTICIPANTES_MATRIZ
~~~
* 8 - BigQuery aceita funcões em JavaScript e SQL _(Preferencialmente)_
~~~sql
CREATE TEMP FUNCTION nomeCompleto_SQL
(
    nome STRING, 
    sobrenome STRING
) 
AS 
((
    SELECT CONCAT("Feito por SQL: ", nome, " ", sobrenome) AS NOME_COMPLETO
));

SELECT nomeCompleto_SQL("RUBENS", "CURY")

CREATE TEMP FUNCTION nomeCompleto_JS
(
    nome STRING, 
    sobrenome STRING
) 
RETURNS STRING LANGUAGE js
AS
"""
    var nomeCompleto = "Feito por JavaScript: " + nome + " " + sobrenome
    return nomeCompleto
""";

SELECT nomeCompleto_JS("RUBENS", "CURY")
~~~


# Exercícios para depois

1. Total de voos que decolaram do estado de NY com mais de 10 minutos de antecedência

2. Qual o total de aeroportos de chegada [arrival_airport] diferentes?

3. Liste o total de voos por data do voo [date] e companhia aérea [airline].    
Ordene o resultado por data do voo [crescente] e total de voos [decrescente] (do maior pro menor nº de voos).    
Somente os 10 primeiros resultados.

4. Liste por ordem alfabética os DIFERENTES nomes [airline] e respectivos códigos [code] das companhias aéreas que    
aterrissaram no aeroporto "SAN" [arrival_airport] cujo o nome da companhia aérea contenha a palavra "Airlines"

5. Adapte a função em JavaScript disponível em (https://www.geodatasource.com/developers/javascript)    
para ser utilizada no BigQuery. Posteriormente, calcule EM KM utilizando esta função, a distância    
dos aeroportos de partida [departure_airport] e destino [arrival_airport] das companhias aéreas    
utilizando suas respectivas coordenadas de latitude e longitude.    
--    
Por fim, liste os nomes da companhias aéreas [airline], aerorporto de partida [departure_airport],    
o aeroporto de chegada [arrival_airport] e a distância calculada entre eles.    
--    
A lista deve ser ordenada de forma decrescente (da maior para a menor) pela distância calculada    
e NÃO deve ser exibido nenhum nome de companhia aérea igual.    
Traga apenas os 10 primeiros registros.