# 1) Total de voos que decolaram do estado de NY com mais de 10 minutos de antecedência
SELECT 
  COUNT(1) 
FROM 
  `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
WHERE
  departure_state = "NY" AND
  departure_delay < -10


# 2) Qual o total de aeroportos de chegada [arrival_airport] diferentes?
SELECT 
  COUNT(DISTINCT arrival_airport)
FROM
  `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`


# 3) Liste o total de voos por data do voo [date] e companhia aérea [airline]. 
# Ordene o resultado por data do voo [crescente] e total de voos [decrescente] (do maior pro menor nº de voos).
# Somente os 10 primeiros resultados.
WITH
VOOS_MES_ANO AS
(
SELECT 
  date,
  airline,
  COUNT(1) AS total_voos
FROM
  `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax`
GROUP BY
  date,
  airline
)
SELECT * FROM VOOS_MES_ANO ORDER BY date ASC, total_voos DESC LIMIT 10


# 4) Liste por ordem alfabética os DIFERENTES nomes [airline] e respectivos códigos [code] das companhias aéreas que 
# aterrissaram no aeroporto "SAN" [arrival_airport] cujo o nome da companhia aérea contenha a palavra "Airlines"
SELECT 
  DISTINCT c.airline, c.code
FROM
  `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax` AS f
INNER JOIN
  `leega-demo-manserv.curso_bigquery_rmc.airline_id_codes` AS c
ON 
  f.airline_code = c.code
WHERE
  f.arrival_airport = "SAN" AND
  c.airline LIKE "%Airlines%"
ORDER BY
  c.airline



# 5) Adapte a função em JavaScript disponível em (https://www.geodatasource.com/developers/javascript)
# para ser utilizada no BigQuery. Posteriormente, calcule EM KM utilizando esta função, a distância
# dos aeroportos de partida [departure_airport] e destino [arrival_airport] das companhias aéreas
# utilizando suas respectivas coordenadas de latitude e longitude.
# --
# Por fim, liste os nomes da companhias aéreas [airline], aerorporto de partida [departure_airport],
# o aeroporto de chegada [arrival_airport] e a distância calculada entre eles.
# ---
# A lista deve ser ordenada de forma decrescente (da maior para a menor) pela distância calculada
# e NÃO deve ser exibido nenhum nome de companhia aérea igual.
# Traga apenas os 10 primeiros registros.

CREATE TEMP FUNCTION calculaDistancia_JS
(
  departure_lon FLOAT64,
  departure_lat FLOAT64,
  arrival_lon FLOAT64,
  arrival_lat FLOAT64,
  unit STRING
) 
RETURNS FLOAT64 LANGUAGE js
AS
"""
	if ((departure_lat == arrival_lat) && (departure_lon == arrival_lon)) {
		return 0;
	}
	else {
		var rad_lat1 = Math.PI * departure_lat/180;
		var rad_lat2 = Math.PI * arrival_lat/180;
		var theta = departure_lon-arrival_lon;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(rad_lat1) * Math.sin(rad_lat2) + Math.cos(rad_lat1) * Math.cos(rad_lat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist;
	}
""";

WITH DISTANCIAS
AS
(
SELECT 
  c.airline as nome_companhia,
  f.departure_airport,
  f.arrival_airport,
  calculaDistancia_JS(departure_lon, departure_lat, arrival_lon, arrival_lat, "K") AS distancia_km
FROM
  `leega-demo-manserv.curso_bigquery_rmc.flights_jfk_lax` AS f
INNER JOIN
  `leega-demo-manserv.curso_bigquery_rmc.airline_id_codes` AS c
ON 
  f.airline_code = c.code
)
SELECT DISTINCT * FROM DISTANCIAS ORDER BY distancia_km DESC LIMIT 10
